see https://www.keycloak.org/docs/latest/server_development/index.html#_themes

to test theme on the test keycloak instance:
> rsync --exclude .git -a . root@rps-test.idcohorts.net:/lxd/containers/accounts/rootfs/var/lib/keycloak/themes/ansible-theme/

visit https://accounts.rps-test.idcohorts.net/auth/realms/users/account for your testaccount page
