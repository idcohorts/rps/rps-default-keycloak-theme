<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "header">
        ${msg("loginProfileTitle")}
    <#elseif section = "form">
        <form id="kc-update-profile-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">


            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Anrede',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Anrede" class="control-label">${msg("Anrede")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" class="form-control" id="user.attributes.Anrede" name="user.attributes.Anrede" value="${(user.attributes.Anrede!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Titel',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Titel" class="control-label">${msg("Titel")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" class="form-control" id="user.attributes.Titel" name="user.attributes.Titel" value="${(user.attributes.Titel!'')}" />
                </div>
            </div>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="firstName" class="${properties.kcLabelClass!}">${msg("firstName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="firstName" name="firstName" value="${(user.firstName!'')}" class="${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="lastName" class="${properties.kcLabelClass!}">${msg("lastName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="lastName" name="lastName" value="${(user.lastName!'')}" class="${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Ort',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Ort" class="control-label">${msg("Ort")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" class="form-control" id="user.attributes.Ort" name="user.attributes.Ort" value="${(user.attributes.Ort!'')}" />
                </div>
            </div>            

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Institution',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Institution" class="control-label">${msg("Institution")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" class="form-control" id="user.attributes.Institution" name="user.attributes.Institution" value="${(user.attributes.Institution!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Abteilung',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Abteilung" class="control-label">${msg("Abteilung")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" class="form-control" id="user.attributes.Abteilung" name="user.attributes.Abteilung" value="${(user.attributes.Abteilung!'')}" />
                </div>
            </div>


            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="email" class="${properties.kcLabelClass!}">${msg("email")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="email" name="email" value="${(user.email!'')}" class="${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('user.attributes.Telefon',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.Telefon" class="control-label">${msg("Telefon")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" class="form-control" id="user.attributes.Telefon" name="user.attributes.Telefon" value="${(user.attributes.Telefon!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                    </div>
                </div>

                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <#if isAppInitiatedAction??>
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doSubmit")}" />
                    <button class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" type="submit" name="cancel-aia" value="true" />${msg("doCancel")}</button>
                    <#else>
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doSubmit")}" />
                    </#if>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
