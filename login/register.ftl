<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "header">
        ${msg("registerTitle")}
    <#elseif section = "form">
        <form id="kc-register-form" class="${properties.kcFormClass!}" action="${url.registrationAction}" method="post">
            
            <div class="form-group">
               <div class="${properties.kcLabelWrapperClass!}">
                   <label for="user.attributes.Anrede" class="${properties.kcLabelClass!}">Anrede</label>
               </div>

               <div class="${properties.kcInputWrapperClass!}">
                   <input type="text" class="${properties.kcInputClass!}" id="user.attributes.Anrede" name="user.attributes.Anrede" value="${(register.formData['user.attributes.Anrede']!'')}"/>
               </div>
            </div>

            <div class="form-group">
               <div class="${properties.kcLabelWrapperClass!}">
                   <label for="user.attributes.Titel" class="${properties.kcLabelClass!}">Titel</label>
               </div>

               <div class="${properties.kcInputWrapperClass!}">
                   <input type="text" class="${properties.kcInputClass!}" id="user.attributes.Titel" name="user.attributes.Titel" value="${(register.formData['user.attributes.Titel']!'')}"/>
               </div>
            </div>            
            
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="firstName" class="${properties.kcLabelClass!}">${msg("firstName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="firstName" class="${properties.kcInputClass!}" name="firstName" value="${(register.formData.firstName!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="lastName" class="${properties.kcLabelClass!}">${msg("lastName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="lastName" class="${properties.kcInputClass!}" name="lastName" value="${(register.formData.lastName!'')}" />
                </div>
            </div>

          <#if !realm.registrationEmailAsUsername>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('username',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="username" class="${properties.kcLabelClass!}">${msg("username")} (vorname.nachname)</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="username" class="${properties.kcInputClass!}" name="username" value="${(register.formData.username!'')}" autocomplete="username" />
                </div>
            </div>
          </#if>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="email" class="${properties.kcLabelClass!}">${msg("email")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="email" class="${properties.kcInputClass!}" name="email" value="${(register.formData.email!'')}" autocomplete="email" />
                </div>
            </div>

            <#if passwordRequired??>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="password" class="${properties.kcLabelClass!}">${msg("password")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="password" id="password" class="${properties.kcInputClass!}" name="password" autocomplete="new-password"/>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password-confirm',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="password-confirm" class="${properties.kcLabelClass!}">${msg("passwordConfirm")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="password" id="password-confirm" class="${properties.kcInputClass!}" name="password-confirm" />
                </div>
            </div>
            </#if>

            <div class="form-group">
               <div class="${properties.kcLabelWrapperClass!}">
                   <label for="user.attributes.Ort" class="${properties.kcLabelClass!}">Ort</label>
               </div>

               <div class="${properties.kcInputWrapperClass!}">
                   <input type="text" class="${properties.kcInputClass!}" id="user.attributes.Ort" name="user.attributes.Ort" value="${(register.formData['user.attributes.Ort']!'')}"/>
               </div>
            </div>

            <div class="form-group">
               <div class="${properties.kcLabelWrapperClass!}">
                   <label for="user.attributes.Institution" class="${properties.kcLabelClass!}">Institution</label>
               </div>

               <div class="${properties.kcInputWrapperClass!}">
                   <input type="text" class="${properties.kcInputClass!}" id="user.attributes.Institution" name="user.attributes.Institution" value="${(register.formData['user.attributes.Institution']!'')}"/>
               </div>
            </div>

            <div class="form-group">
               <div class="${properties.kcLabelWrapperClass!}">
                   <label for="user.attributes.Abteilung" class="${properties.kcLabelClass!}">Abteilung</label>
               </div>

               <div class="${properties.kcInputWrapperClass!}">
                   <input type="text" class="${properties.kcInputClass!}" id="user.attributes.Abteilung" name="user.attributes.Abteilung" value="${(register.formData['user.attributes.Abteilung']!'')}"/>
               </div>
            </div>





            <div class="form-group">
               <div class="${properties.kcLabelWrapperClass!}">
                   <label for="user.attributes.Telefon" class="${properties.kcLabelClass!}">Telefon</label>
               </div>

               <div class="${properties.kcInputWrapperClass!}">
                   <input type="text" class="${properties.kcInputClass!}" id="user.attributes.Telefon" name="user.attributes.Telefon" value="${(register.formData['user.attributes.Telefon']!'')}"/>
               </div>
            </div>


            <#if recaptchaRequired??>
            <div class="form-group">
                <div class="${properties.kcInputWrapperClass!}">
                    <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                </div>
            </div>
            </#if>

            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                        <span><a href="${url.loginUrl}">${kcSanitize(msg("backToLogin"))?no_esc}</a></span>
                    </div>
                </div>

                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doRegister")}"/>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
